# README #

Assignments for Front-End JavaScript Frameworks: Introduction to AngularJS taught by Prof. Jogesh K. Muppala, Hong Kong University on Coursera: Jan 11th - Feb 8th, 2016.


### Topics covered in Week 2 ###
* Overview of Task runners, 
* Web Tools: Grunt and Gulp, 
* Angular scope,
* Angular forms and form validation

### Tools and Softwares (Week 2) ###

* AngularJS
* Bootstrap
* Bower
* Grunt
* Gulp


### Topics covered in Week 3 ###
* Dependency Injection
* Angular Services
* Angular Factory
* Angular Templates
* Angular ngRoute
* Angular UI-Router
* Single Page Applications

### Tools and Softwares (Week 3) ###

* AngularJS
* Bootstrap
* Brackets
* Bower
* angular-route
* angular-ui-router


### Topics covered in Week 4 ###
* Client-Server Communication (Networking Essentials)
* Angular $http Service
* RESTful Services - Angular ngResource ($resource)
* Unit Testing in Angular
* End-to-end Testing in Angular
* Web Tools: Yo and Yeoman

### Tools and Softwares (Week 4) ###

* AngularJS
* Bootstrap
* Brackets
* json-server
* angular-resource
* JASMINE
* KARMA and PhantomJS
* angular-mocks
* Protractor
* Yo


### Who do I talk to? ###

* Vignesh Subramanian


All works presented are original.